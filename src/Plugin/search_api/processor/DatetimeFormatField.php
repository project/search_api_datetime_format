<?php

namespace Drupal\search_api_datetime_format\Plugin\search_api\processor;

use Drupal\search_api_datetime_format\Plugin\search_api\processor\Property\DatetimeFormatFieldProperty;

use Drupal\search_api\Datasource\DatasourceInterface;
use Drupal\search_api\Processor\ProcessorPluginBase;
use Drupal\search_api\Item\ItemInterface;
use Drupal\search_api\Utility\Utility;

/**
 * Datetime format field
 *
 * @see \Drupal\search_api_datetime_format\Plugin\search_api\processor\Property\GranulatedFieldProperty
 *
 * @SearchApiProcessor(
 *   id = "datetime_format_field",
 *   label = @Translation("Datetime format field"),
 *   description = @Translation(""),
 *   stages = {
 *     "add_properties" = 20,
 *   },
 *   locked = true,
 *   hidden = true,
 * )
 */
class DatetimeFormatField extends ProcessorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getPropertyDefinitions(DatasourceInterface $datasource = NULL) {
    $properties = [];

    if (!$datasource) {
      $definition = [
        'label' => $this->t('Datetime format field'),
        'description' => $this->t(''),
        'type' => 'string',
        'is_list' => false,
        'processor_id' => $this->getPluginId(),
      ];
      $properties['datetime_format_field'] = new DatetimeFormatFieldProperty($definition);
    }

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function addFieldValues(ItemInterface $item) {
    $datetime_format_fields = $this->getFieldsHelper()
      ->filterForPropertyPath($item->getFields(), NULL, 'datetime_format_field');

    foreach ($datetime_format_fields as $datetime_format_field) {
      $values = [];

      $configuration = $datetime_format_field->getConfiguration();

      $datetime_field = $configuration['datetime_field'];
      $format = $configuration['format'];

      list(, $property_path) = Utility::splitCombinedId($datetime_field);

      $required_properties = [
        $item->getDatasourceId() => [
          $property_path => 'value',
        ],
      ];

      $item_values = $this->getFieldsHelper()
        ->extractItemValues([$item], $required_properties);

      foreach ($item_values as $key => $dates) {
        $value = $dates['value'];

        for ($i = 0, $n = count($value); $i < $n; $i++) {
          $v= $value[$i];
          if (!is_numeric($v)) {
            $v = strtotime($v);
          }
          $values[] = date($format, $v);
        }
      }

      // Do not use setValues(), since that doesn't preprocess the values
      // according to their data type.
      foreach ($values as $value) {
        $datetime_format_field->addValue($value);
      }
    }
  }
}
