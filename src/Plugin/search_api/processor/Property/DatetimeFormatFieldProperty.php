<?php

namespace Drupal\search_api_datetime_format\Plugin\search_api\processor\Property;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Html;
use Drupal\search_api\Processor\ConfigurablePropertyBase;
use Drupal\search_api\Item\FieldInterface;
use Drupal\search_api\Utility\Utility;

/**
 * Defines an "datetime format field" property.
 *
 * @see \Drupal\search_api_datetime_format\Plugin\search_api\processor\DatetimeFormatField
 */
class DatetimeFormatFieldProperty extends ConfigurablePropertyBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getFieldDescription(FieldInterface $field) {
    $configuration = $field->getConfiguration();

    $arguments = [
      '@field' => $configuration['datetime_format_field'] ?? '',
    ];

    return $this->t('A field that format Datetime: @field.', $arguments);
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(FieldInterface $field, array $form, FormStateInterface $form_state) {
    $form['datetime_field'] = [
      '#type' => 'radios',
      '#title' => $this->t('Source Datetime Field'),
      '#default_value' => $field->getConfiguration()['datetime_field'],
      '#options' => $this->getAvailableDatetimeProperties($field),
      '#required' => TRUE,
    ];

    $form['format'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Format'),
      '#default_value' => $field->getConfiguration()['format'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(FieldInterface $field, array &$form, FormStateInterface $form_state) {
    $values = [
      'datetime_field' => $form_state->getValue('datetime_field'),
      'format' => $form_state->getValue('format'),
    ];
    $field->setConfiguration($values);
  }

  /**
   * Retrieve all properties that the field type is datetime available on the
   * index.
   *
   * The properties will be keyed by combined ID, which is a combination of the
   * datasource ID and the property path. This is used internally in this class
   * to easily identify any property on the index.
   *
   * @param \Drupal\search_api\Item\FieldInterface $field
   *   The field
   *
   * @return string[]
   *   All the available properties on the index, keyed by combined ID.
   *
   * @throws \Drupal\search_api\SearchApiException
   * @see \Drupal\search_api\Utility::createCombinedId()
   */
  protected function getAvailableDatetimeProperties(FieldInterface $field) {
    $field_options = [];

    $index = $field->getIndex();

    $datasources = $index->getDatasources();

    foreach ($datasources as $datasource_id => $datasource) {
      foreach ($index->getPropertyDefinitions($datasource_id) as $property_path => $property) {
        $combined_id = Utility::createCombinedId($datasource_id, $property_path);

        if (!$property instanceof \Drupal\field\Entity\FieldConfig) {
          continue;
        }

        if ($property->getFieldStorageDefinition()->getType() != 'datetime') {
          continue;
        }

        $label = $datasource->label() . ' » '. $property->getLabel();

        $field_options[$combined_id] = Html::escape($label);
      }
    }

    return $field_options;
  }
}
